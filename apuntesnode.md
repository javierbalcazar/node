## Comeenzar un repositorio nuevo y subirlo a bitbucket
primero crear un archivo .gitignore con node_modules/
segundo git init
tercero git add . para añadir todo
cuarto git commit -m "Mensaje a poner"
quinto git push origin master para subir todo al repo


## Crear un servidor
En algún lugar dentro de Node.js vive un módulo llamado "http", y podemos hacer uso de éste en nuestro propio código requiriéndolo y asignando el resultado del requerimiento a una variable.

Esto transforma a nuestra variable en un objeto que incluye todos los métodos públicos que el módulo http provee.

Para iniciar un servidor necesitamos el módulo http que viene incluido con node. Para hacerlo accesible con todos sus métodos, hay que guardarlo en una variable.

```js
const http = require("http");
```

Ahora para crear el servidor hay que crear una constante llamada server con las respuestas que queramos darle cuando haya una petición.

```js
const http = require("http"); //Requerimos el módulo http para crear el servidor a continuación

const server = http.createServer((request, response) => {
    console.log("Peticion Recibida."); //Esto saltará cuando haya una petición, es decir, cuando se ponga localhost:2222
    response.writeHead(200, {"content-Type": "text/html"}); //Código de respuesta que queramos dar
    response.write("<h1>Mi primer servidor con Node JS</h1>"); //Con este método, escribirmos en el html
    response.end(); //Finalizar la respuesta
    
})

console.log("Servidor 2 iniciado"); //Cuando inidicemso el servidor con node server.js saltará por conola

server.listen(2222); //Puerto en el ser el servidor escuchará para hacer saltar la constante server
```

## Que son los módulos y cómo se instalan
En NodeJS el código se organiza por medio de módulos

## Cómo crear un módulo y exportarlo
Podemos crear nuestros propios módulos y exportar sus funciones
```js
//Creamos una carpeta llamada libs y ahí un archivo js saludar
const saludar = (nombre) => {
    return `Hola ${nombre}`
}

module.exports = saludar  //Exportamos la función poniendo su nombre

//Creamos en la raíz otro archivo llamado app.js y lo llamamos con require
const saludar = require("./libs/saludar")

saludar("javi")  //Lo podemos utilizar directamente
```

## Inicio api
1º npm init (crear packcage.json y package-lock.json)
2º npm install express (instala node_modules)
3º npm install (esto es por si acaso)



