const fs = require("fs")
const express = require("express")
const bodyParser = require("body-parser")

//INICIAR EXPRESS
const app = express()

// CONFIGURACION: traducimos el json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//GET todos los pokemons
app.get("/api/pokemons", (request,response) => {
    fs.readFile("./db/dbPokemon.json", (error, data) => {
        const allPokemons = JSON.parse(data)
        if(error){
            response.status(404).send("Lo siento página no encontrada")
            response.end()
        }
        else{
        response.status(200).send({
            success: "true",
            message: "Pokedex ecnontrada con estos pokemons",
            pokemons: allPokemons
            })
            response.end()
        }
    })
})


//GET ONE pokemon por parámetros
app.get("/api/pokemons/:id", (request, response) =>{
    fs.readFile("./db/dbPokemon.json", (error, data) => {
        
        const allPokemons = JSON.parse(data)

        let onePokemon = allPokemons.filter(pokemon => parseInt(pokemon.id) === parseInt(request.params.id))       

        if(error){
            response.status(404).send({
                success: "false",
                message: error
            })
            console.error("Ha ocurrido un error")
        }
        else{
            response.status(200).send({
                success: "true",
                message: "Pokemon encontrado",
                pokemon: onePokemon
            })
        }
    })
})

//POST ONE pokemon
app.post("/api/pokemons", (request, response) => {
    fs.readFile("./db/dbPokemon.json", "utf-8", (error, data) => {

        const allPokemons = JSON.parse(data)
        const newPokemon = {
            id: request.body.id,
            type: request.body.type,
            name: request.body.name
        }

        allPokemons.push(newPokemon)

        fs.writeFile("./db/dbPokemon.json", JSON.stringify(allPokemons), (error) => {
            if(error){
                response.status(404).send({
                    success: "false",
                    message: error
                })
            }
            else{
                response.status(200).send({
                    success: "true",
                    message: "Pokemon añadido",
                    nuevoPokemon: newPokemon
                })
            }
        })

    })
})

//DELETE ONE Pokemon por parámetro
app.delete("/api/pokemons/:id", (request, response) => {
    fs.readFile("./db/dbPokemon.json", "utf-8", (error, data) => {
        let allPokemons = JSON.parse(data)
        const deletedPokemon = allPokemons.filter(pokemon => parseInt(pokemon.id) === parseInt(request.params.id))
        let newList = allPokemons.filter(pokemon => pokemon.id != request.params.id)

        fs.writeFile("./db/dbPokemon.json", JSON.stringify(newList), (error) => {
            if(error){
                response.status(404).send({
                    success: "False",
                    message: error
                })
            }
            else{
                response.status(200).send({
                    success: "True",
                    message: "Pokemon borrado",
                    borrado: deletedPokemon,
                    listadoActual: newList
                })
            }
        })

    })
})

//PUT ONE Pokemon por parámetros
app.put("/api/pokemons/:id", (request, response) =>{
    fs.readFile("./db/dbPokemon.json", (error, data) =>{

        let allPokemons = JSON.parse(data)

        if(!request.body.name){
            return response.status(404).send({
                success: "false",
                message: "Falta el name",
                error: error
            })
        }
        else if(!request.body.type){
            return response.status(404).send({
                success: "false",
                message: "Falta el type",
                error: error
            })
        }


        for(let index in allPokemons){
            if(request.params.id == allPokemons[index].id){
                if(request.body.type){
                    allPokemons[index].type = request.body.type
                }
                if(request.body.name){
                    allPokemons[index].name = request.body.name
                }
            }

        }

        fs.writeFile("./db/dbPokemon.json", JSON.stringify(allPokemons), (error) =>{
            if(error){
                return response.status(404).send({
                    success: "false",
                    message: error
                })
            }
            else{
                return response.status(200).send({
                    success: "true",
                    message: "Pokemon modificado",
                    listado: allPokemons
                })
            }
        })

    })
})

app.listen(1212)
console.log("api iniciada")