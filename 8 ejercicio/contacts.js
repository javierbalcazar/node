const express = require("express");
const fs = require("fs");
const bodyParser = require("body-parser");
const app = express();
const funcionlog = require("./libs/funcionlog")


//Esto es el GET
app.get("/api/contacts", (request, response) => {
    funcionlog.funcionlog(request)
    
    fs.readFile("db/dbContacts.json", "utf-8", (error, data) => {
        response.status(200).send({
            succes: true,
            message: "patatas",
            contacts: JSON.parse(data), //Esto convierte el listado de contactos en un JSON legible
        });
    });
});

//JSON.stringify se usa en el writeFile y JSON.parse se usa en el readFile

//Configuraciones necesarias al trabajar con JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

//Este es el post
app.post("/api/contacts", (request, response) => {
    funcionlog.funcionlog(request)
    if(!request.body.name || !request.body.surname) {
        return response.status(400).send(
            {
                succes: false,
                message: "Por favor, introduzca los datos que faltan",
            }
        );
    }
    fs.readFile("db/dbContacts.json", "utf-8", (error, data) => {
        if(error) throw error
        let allContacts = JSON.parse(data);
        const newContact = {
            id: allContacts.length + 1,
            name: request.body.name,
            surname: request.body.surname,
            email: request.body.email
        };
        allContacts.push(newContact);
        fs.writeFile("db/dbContacts.json", JSON.stringify(allContacts), (error) => { 
            if(error) throw error
            console.log("Contacto añadido");
            return response.status(200).send(
            {
                    succes: true,
                    message: "Contacto añadido",
                    newContact
            }
        );
        });
    });
});
app.delete("/api/contacts", (request, response) => {
    funcionlog.funcionlog(request)
    fs.readFile("db/dbContacts.json", "utf-8", (error, data) => {
        if(error) throw error
        let allContacts = JSON.parse(data);
        const deleteContact = {
            id: allContacts.length - 1,
            name: request.body.name,
            surname: request.body.surname,
            email: request.body.email
        };
        allContacts.pop(deleteContact);
        fs.writeFile("db/dbContacts.json", JSON.stringify(allContacts), (error) => {
            if(error) throw error
            console.log("Contacto eliminado");
            return response.status(200).send(
                {
                    succes: true,
                    message: "Contacto eliminado",
                    deleteContact
                }
            );
            });
        });
    });
app.listen(1989);
console.log("Servidor escuchando en el puerto 1989");

