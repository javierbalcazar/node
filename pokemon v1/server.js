const http = require("http");
const fs = require("fs");

const server = http.createServer((request, response) => {

    let path = "";
    
     if(request.url === "/getPokemon") {
        path = "src/html" + request.url + ".html"
     }
     if(request.url === "/postPokemon") {
        path = "src/html" + request.url + ".html"
     }

     /*A continuaación path recibe el valor del if de arriba. Si por ejemplo el usuario pone en el navegador
     http://localhost:3333/user el path será "src/html" + request.url + ".html", es decir, src/html/user.html.
     Con el response.write(data) se pinta lo que haya en ese path que está recibiendo*/
     
    fs.readFile(path, (error, data) => {
        if(error) {
            console.error(error.message);
            response.writeHead(404);
            response.write("Error: Página no encontrada")
            response.end();
        }
        else {
            response.writeHead(200);
            response.write(data); //Pinta lo que haya en el path que recibe la callback
            response.end();
        }
        
});
});


const PORT = 4000;

server.listen(PORT, function () {
  console.log(`SERVER corriendo en puerto ${PORT}`);
});
