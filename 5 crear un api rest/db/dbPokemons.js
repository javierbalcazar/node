const pokemons = [
    {
        id: 1,
        name: "Charizard",
        type: "Fire"
    },
    {
        id: 2,
        name: "Pikachu",
        type: "Electric"
    },
    {
        id: 3,
        name: "Squirtle",
        type: "Water"
    }
];

const suma = (x, y) => x + y;

module.exports = {
    pokemons: pokemons,
    suma: suma
}