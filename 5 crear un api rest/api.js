const express = require("express");
const fs = require("fs");
const bodyParser = require("body-parser"); //Sirve para parsear bodys json para que express entienda
const db = require("./db/dbPokemons");

const app = express(); // Creacion de API



//crear un método get
app.get("/api/pokemons", (request, response) => {

    fs.readFile(".db/dbPokemon.json", "utf-8", (error, data) => {
        response.status(200).send({
            success: true,
            message: "Mi primera api es una Pokedex",
            pokemons: data //Array de objetos con id, name, type
        });
    })
    
    response.status(200).send({
        success: true,
        message: "Mi primera api es una Pokedex",
        pokemons: db.pokemons //Array de objetos con id, name, type
    });
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

app.post("/api/pokemons", (request, response) => {

    if(!request.body.name || !request.body.type) {
        return response.status(400).send(
            {
                success: false,
                message: "faltan parámetros"
            }
        )
    }

    const newPokemon = {
        id: db.pokemons.length + 1,
        name: request.body.name,
        type: request.body.type
    };

    db.pokemons.push(newPokemon)


    return response.status(200).send(
        {
            success: true,
            message: "Pokemon añadido",
            newPokemon
        }
    )
});

app.listen(5555);
console.log("Servidor escuchando en el puerto 5555");
console.log(db.suma(2,3))