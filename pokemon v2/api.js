const express = require('express');
const fs = require("fs");
const log = require("./log");
const bodyParser = require('body-parser');

// Inicia express
const app = express();

// CONFIGURACION: CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');

  // authorized headers for preflight requests
  // https://developer.mozilla.org/en-US/docs/Glossary/preflight_request
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();

  app.options('*', (req, res) => {
    // allowed XHR methods  
    res.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
    res.send();
  });
});

// CONFIGURACION: traducimos el json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// GET LOCATIONS
app.get('/api/pokemons/:idPokemon/locations/:idLocation', (req, res) => {
  fs.readFile("db/dbPokemon.json", "utf-8", (error, data) => {

    const allPokemons = JSON.parse(data)

    const idPokemon = req.params.idPokemon
    const idLocation = req.params.idLocation

    const pokemon = allPokemons.filter(pokemon => pokemon.id === parseInt(idPokemon)) //Esto devuelve un array de 1 elemento
    const location = pokemon[0].locations.filter(location => location.id === parseInt(idLocation))

    res.status(200).send({
      success: 'true',
      message: 'Pokedex',
      idPokemon: idPokemon,
      idLocation: idLocation,
      location: location
    });
  });
});

// GET PAGINATION pokemons
app.get('/api/pokemons/page/:page', (req, res) => {
  fs.readFile("db/dbPokemon.json", "utf-8", (error, data) => {

    let allPokemons = JSON.parse(data);

    const pageSize = 5;
    const page = parseInt(req.params.page);

    let end = page * pageSize

    let init = end - pageSize

    const pagePokemons = allPokemons.slice(init, end)

    const numPages = Math.ceil(allPokemons.length / pageSize)

    res.status(200).send({
      success: 'true',
      message: 'Pokedex',
      page: page,
      pagesize: pagePokemons.length,
      numPages: numPages,
      pokemons: pagePokemons
    });
  });
});

// GET ALL pokemons
app.get('/api/pokemons', (req, res) => {
  fs.readFile("db/dbPokemon.json", "utf-8", (error, data) => {
    const allPokemons = JSON.parse(data);
    res.status(200).send({
      success: 'true',
      message: 'Pokedex',
      pokemons: allPokemons
    });
  });
});


// GET ONE pokemons params
app.get('/api/pokemons/:id', (req, res) => {
  log.writeLog(req);
  fs.readFile("db/dbPokemon.json", "utf-8", (error, data) => {
    
    const allPokemons = JSON.parse(data);

    const id = req.params.id;
    const pokemon = allPokemons.filter(pokemon => pokemon.id === parseInt(id));

    res.status(200).json({
      success: 'true',
      message: 'Pokedex',
      pokemon: pokemon
    });
  });
});

// GET ONE pokemons querystring
app.get('/api/pokemon/', (req, res) => {
  log.writeLog(req);
  fs.readFile("db/dbPokemon.json", "utf-8", (error, data) => {
    
    const allPokemons = JSON.parse(data);

    const id = req.query.id;
    const pokemon = allPokemons.filter(pokemon => pokemon.id === parseInt(id));

    res.status(200).send({
      success: 'true',
      message: 'Pokedex',
      pokemon: pokemon
    });
  });
});



// PUT ONE pokemon
app.put('/api/pokemons/:id', (req, res) => {
  log.writeLog(req);
  fs.readFile("db/dbPokemon.json", "utf-8", (error, data) => {
    const allPokemons = JSON.parse(data);

    allPokemons.forEach(element => {
      if (element.id === Number.parseInt(req.params.id)) {
        element.name = req.body.name ? req.body.name : element.name;
        element.type = req.body.type ? req.body.type : element.type;
      }
    });

    fs.writeFile('db/dbPokemon.json', JSON.stringify(allPokemons), (err) => {
      // If an error occurred, show it and return
      if (err) return console.error(err);

      // CODIGO: Con respuesta de un objeto
      return res.status(201).send({
        success: 'true',
        message: 'Pokemon modificado',
      });
    });
  });
});



// POST pokemons
app.post('/api/pokemons', (req, res) => {
  log.writeLog(req);
  if (!req.body.name) {
    return res.status(400).send({
      success: 'false',
      message: 'name is required'
    });
  } else if (!req.body.type) {
    return res.status(400).send({
      success: 'false',
      message: 'type is required'
    });
  }

  fs.readFile("db/dbPokemon.json", "utf-8", (error, data) => {

    const allPokemons = JSON.parse(data);

    const newPokemon = {
      id: allPokemons.length + 1,
      name: req.body.name,
      type: req.body.type
    };
    allPokemons.push(newPokemon);

    fs.writeFile('db/dbPokemon.json', JSON.stringify(allPokemons), (err) => {
      // If an error occurred, show it and return
      if (err) return console.error(err);

      // CODIGO: Con respuesta de un objeto
      return res.status(201).send({
        success: 'true',
        message: 'Pokemon añadido',
        newPokemon
      });
    });

  })

});


// DELETE pokemons
app.delete('/api/pokemons/:id', (req, res) => {
  log.writeLog(req);
  if (!req.params.id) {
    return res.status(400).send({
      success: 'false',
      message: 'id is required'
    });
  }

  fs.readFile("db/dbPokemon.json", "utf-8", (error, data) => {

    // Con variables
    let allPokemons = JSON.parse(data);
    const deletePokemon = {
      id: Number.parseInt(req.params.id)
    };
    allPokemons = allPokemons.filter(
      pokemon => pokemon.id !== deletePokemon.id
    );

    fs.writeFile('db/dbPokemon.json', JSON.stringify(allPokemons), (err) => {
      // If an error occurred, show it and return
      if (err) return console.error(err);
      // Successfully wrote to the file!

      return res.status(200).send({
        success: 'true',
        message: 'Pokemon borrado',
        deletePokemon
      });
    });
  })
});


const PORT = 5000;
app.listen(PORT, function () {
  console.log(`API corriendo en puerto ${PORT}`);
});