const fs = require("fs");

const writeLog = (request) => {
    const log = `${request.method}|${request.headers.host}|${new Date().toLocaleTimeString()}\n`; 
    fs.appendFile('./logs/pokemonServer.log', log, (error) => {
      if (error) throw error;
      console.log('The "data to append" was appended to file!');
    });  
}

module.exports= {
    writeLog: writeLog
  };
  