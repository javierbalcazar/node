const http = require("http");
const fs = require("fs");

const server = http.createServer((request, response) => {

    // Cargar un html especifico dependiendo de la url que ponga el cliente
    // Como saber la url?? request.url

    let path = "";
    if(request.url === "/getPokemon") {
        path = "src/html" + request.url + ".html";
    }
    if(request.url === "/postPokemon") {
        path = "src/html" + request.url + ".html";
    }
    if(request.url === "/paginationPokemon") {
        path = "src/html" + request.url + ".html"
    }
    
    // src/html/home.html
    fs.readFile(path, (error, data) => {
        if(error) {
            console.error(error.message);
            response.writeHead(404);
            response.write("Error: Pagina no encontrada");
            response.end();
        } else {
            response.writeHead(200);
            response.write(data);
            response.end();
        }
    });
});


const PORT = 4000;
server.listen(PORT, function () {
  console.log(`Server corriendo en puerto ${PORT}`);
});