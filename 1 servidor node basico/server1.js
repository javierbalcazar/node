const http = require("http");

const server = http.createServer((request, response) => {
    response.writeHead(200, {"content-Type": "text/html"});
    response.write("<h1>Mi primer servidor con Node JS</h1>");
    response.end();

});
const host = "127.0.0.1"
const port = "1234"

server.listen(port, host, () => {
    console.log(`Servidor 1 iniciado en el puerto http://${host}:${port}`);
});
