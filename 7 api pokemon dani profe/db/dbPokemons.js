const pokemons = [{
    id: 1,
    name: "Bulbasur",
    type: "planta"
},{
    id: 2,
    name: "Squirtle",
    type: "agua"
},{
    id: 3,
    name: "Charmander",
    type: "fuego"
}
];

module.exports = {
    pokemons: pokemons,
}