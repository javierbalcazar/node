const express = require("express");
const fs = require("fs");
const bodyParser = require("body-parser");
const app = express(); // Creacion de API
//Crear un metodo GET
app.get("/api/pokemons", (request, response) => {
    const log = request.method + "|" + request.path + "|" + new Date().toLocaleTimeString() + "\n";
    fs.appendFile("log/pokemon.log",log, (error) => {
        if(error) throw error;
    });
    fs.readFile("db/dbPokemon.json", "utf-8", (error, data) => {
        response.status(200).send({
            success: true,
            message: "Mi primera API: Pokedex",
            pokemons: JSON.parse(data)
        });
    });
});
// traducimos el body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : false}));
app.post("/api/pokemons", (request, response) => {
    // const log = `${request.method}|${request.path}|${new Date().toLocaleTimeString()}\n`;
    const log = request.method + "|" + request.path + "|" + new Date().toLocaleTimeString() + "\n";
    fs.appendFile("log/pokemon.log",log, (error) => {
        if(error) throw error;
    });
    // Si no me viene el name o el type
    if(!request.body.name || !request.body.type) {
        return response.status(400).send(
            {
                success: false,
                message: "Faltan parametros",
            }
        );
    }
    fs.readFile("db/dbPokemon.json", "utf-8", (error, data) => {
        if(error) throw error
        let allPokemons = JSON.parse(data);
        const newPokemon = {
            id: allPokemons.length + 1,
            name: request.body.name,
            type: request.body.type
        };
        allPokemons.push(newPokemon);
        fs.writeFile("db/dbPokemon.json", JSON.stringify(allPokemons), (error) => {
            if(error) throw error
           console.log("Pokemon añadido") 
           return response.status(200).send(
            {
                success: true,
                message: "Pokemon añadido",
                newPokemon
            }
        );
        });
    });
});
app.listen(5555);
console.log("Servidor escuchando en el puerto 5555");