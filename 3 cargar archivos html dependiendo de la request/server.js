const http = require("http"); //traer librerias o módulos y usar funcionalidades de node
const fs = require("fs"); //traer librerias o módulos y usar funcionalidades de node

const server = http.createServer((request, response) => {

    let path = "";
    
     if(request.url === "/home") {
        path = "src/html" + request.url + ".html"
     }
     else if(request.url === "/user") {
        path = "src/html" + request.url + ".html"
     }
     else if(request.url === "/login"){
        path = "src/html" + request.url + ".html"
     }

     /*A continuaación path recibe el valor del if de arriba. Si por ejemplo el usuario pone en el navegador
     http://localhost:3333/user el path será "src/html" + request.url + ".html", es decir, src/html/user.html.
     Con el response.write(data) se pinta lo que haya en ese path que está recibiendo*/
     
    fs.readFile(path, (error, data) => {  //Intenta leer lo que viene en el path.
        if(error) {                     //Si el path viene vacío o no encuentra un fichero llamado como el path, salta el error
            console.error(error.message);
            response.writeHead(404);
            response.write("Error: Página no encontrada")
            response.end();
        }
        else {
            response.writeHead(200);
            response.write(data); //Pinta lo que haya en el archivo html que se corresponda con el path que recibe la callback
            response.end();
        }
        

});

});

server.listen(3333)
console.log("Servidor 3 iniciado")