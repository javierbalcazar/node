const http = require("http")
const fs = require("fs")

const server = http.createServer((request, response) => {

    let path = "";

    if(request.url === "/getcontact"){
        path = "./src" + request.url + ".html"
    }
    if(request.url === "/putcontact"){
        path = "./src" + request.url + ".html"
    }

    fs.readFile(path, (error, data) => {
        if(error){
            console.error(error);
            response.writeHead(400);
            response.write("Error. Página no encontrada")
            response.end();
        }
        else{
            response.writeHead(200);
            response.write(data);
            response.end();
        }
    })

})

server.listen(2020)
console.log("Server corriendo en el puerto 2020")