const fs = require("fs");
const express = require("express");
const bodyParser = require("body-parser");

const app = express()

// CONFIGURACION: CORS para que nuestro webservice pueda accede a la api pública
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    // authorized headers for preflight requests
    // https://developer.mozilla.org/en-US/docs/Glossary/preflight_request
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
    app.options('*', (req, res) => {
      // allowed XHR methods  
      res.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
      res.send();
    });
  });

// CONFIGURACION: traducimos el json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//GET un contacto sin parámetro
app.get("/api/contacts", (request, response) => {
    fs.readFile("./db/dbContacts.json", (error, data) => {

        const agenda = JSON.parse(data)
        let contacto = ""

        contacto = agenda.filter(contacto => parseInt(request.body.id) === parseInt(contacto.id))

        if(error){
            response.status(404).send({
                success: "False",
                message: error
            })
        }
        else{
            response.status(200).send({
                success: "True",
                message: "Contacto encontrado",
                contacto: contacto
            })
        }
    })
})

//GET con parámetro
app.get("/api/contacts/:id", (request, response) => {
    fs.readFile("./db/dbContacts.json", (error, data) => {

        const agenda = JSON.parse(data);
        let contacto = "";

        contacto = agenda.filter(contacto => parseInt(request.params.id) === parseInt(contacto.id))

        if(error){
            response.status(404).send({
                success: "False",
                message: error
            })
        }
        else{
            response.status(200).send({
                success: "True",
                message: "Contacto econtrado",
                contacto: contacto
            })
        }
    })
})

//POST. Añadir un contacto nuevo
app.post("/api/contacts", (request, response) => {
    fs.readFile("./db/dbContacts.json", (error, data) => {

        if(error){
            response.status(404).send({
                success: "False",
                message: error
            })
        }

        let agenda = JSON.parse(data);

        let newContact = {
            id: request.body.id,
            name: request.body.name,
            email: request.body.email
        }

        agenda.push(newContact)

        fs.writeFile("./db/dbContacts.json", JSON.stringify(newContact), (error) => {
            if(error){
                response.status(404).send({
                    success: "False",
                    message: error
                })
            }
            else{
                response.status(200).send({
                    success: "True",
                    message: "Contacto añadido",
                    agenda: agenda
                })
            }
        })

    })
})

//PUT con parámetro. Modificar un contacto
app.put("/api/contacts/:id", (request, response) => {
    fs.readFile("./db/dbContacts.json", (error, data) => {

        let agenda = JSON.parse(data)

        for(let index in agenda){
            if(parseInt(agenda[index].id) === parseInt(request.params.id)){
                agenda[index].name = request.body.name;
                agenda[index].email = request.body.email;
            }
        } 
        
        fs.writeFile("./db/dbContacts.json", JSON.stringify(agenda), (error) => {
            if(error){
                response.status(404).send({
                    success: "False",
                    message: error
                })
            }
            else{
                response.status(200).send({
                    success: "True",
                    message: "Contacto modificado",
                    agenda: agenda
                })
            }
        })

    })
})

//DELETE con parámetro. Borrar un contacto según el id
app.delete("/api/contacts/:id", (request, response) => {
    fs.readFile("./db/dbContacts.json", (error, data) =>{
        let agenda = JSON.parse(data);

        agenda = agenda.filter(contacto => parseInt(contacto.id) !== parseInt(request.params.id))

        fs.writeFile("./db/dbContacts.json", JSON.stringify(agenda), (error) => {
            if(error){
                response.status(404).send({
                    success: "False",
                    message: error
                })
            }
            else{
                response.status(200).send({
                    success: "True",
                    agenda: agenda
                })
            }
        })
    })
})

app.listen(1010)
console.log("API iniciada")