const http = require("http");
const fs = require("fs");

const server = http.createServer((request, response) => {
    // cargar el html
    fs.readFile("index.html", (error, data) => {
        if(error) {
            console.error(`La carga del fichero ha fallado. El código de error ${error.code} ${error.message}`);
            response.writeHead(404);
            response.write("Página no encontrada")
        }
        else {
        response.writeHead(200);
        response.write(data);
        }
        response.end();
    });
});


server.listen(2222);
console.log("Servidor 2 iniciado");