const http = require("http");
const fs = require("fs");

const server = http.createServer((request, response) => {

    let path = ""

    if(request.url === "/home"){
        path = "./src" + request.url + ".html"
    }
    else if(request.url === "/user"){
        path = "./src" + request.url + ".html"
    }

    fs.readFile(path, (error, data) => {
        if(error){
            response.writeHead(404)
            console.error(error)
            console.log("Página no encontrada")
            response.end()
        }
        else{
            response.writeHead(200)
            response.write(data)
            console.log("Página encontrada")
            response.end()
        }
    })

})

server.listen(2121)
console.log("servidor iniciado")